//=require ../blocks/**/*.js
$(document).ready(function () {

    // open modal
    $('.js-open-swap').click(function () {
        $('.js-modal-swap').fadeIn();
        $('.js-mask').fadeIn();
        $('body').addClass('overflov');
    });

    //close modal
    $('.js-mask, .js-modal-close').click(function () {
        $('.js-modal').fadeOut();
        $('.js-mask').fadeOut();
        $('body').removeClass('overflov');
    });
   
});

